package br.edu.up.clieureka;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@EnableEurekaClient
@SpringBootApplication
public class ClienteEurekaApp {
  
	public static void main(String[] args) {
		SpringApplication.run(ClienteEurekaApp.class, args);
	}

  @RequestMapping("/")
	public String home(){
		return "Cliente Eureka: cli-eureka";
	}

  @Autowired
  DiscoveryClient discoveryClient;

  @RequestMapping("/services/{applicationName}")
	public List<ServiceInstance> getServicesByApplicationName(
			@PathVariable String applicationName) {
		return this.discoveryClient.getInstances(applicationName);
	}
}

/**
 * Exemplo de utilização do DiscoveryClient, EurekaCliente e RestTemplate 
 * para encontrar os serviços registrados no servidor Eureka e consumi-los.
 */
@Component
class ExemploDiscoveryServices implements CommandLineRunner {

  @Autowired
  private DiscoveryClient discoveryClient;
  
  @Autowired
  private EurekaClient eurekaClient;
  
  public String serviceUrl(String serviceId) {
      InstanceInfo instance = eurekaClient.getNextServerFromEureka(serviceId, false);
      String url = instance.getHomePageUrl();      
      System.out.println("EurekaClient ----------->: " + url);
      return url;
  }

  @Override
  public void run(String... strings) throws Exception {
    discoveryClient.getInstances("mic-pessoas").forEach((ServiceInstance service) -> {
      System.out.println("\n");  
      System.out.println("DiscoveryClient -----------> " + service.toString());
      System.out.println("\n");  
    });

    final String URL = serviceUrl("mic-pessoas");

    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<List<Pessoa>> exchange =
            restTemplate.exchange(
              URL + "/pessoas",
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<Pessoa>>() {
                    },
                    (Object) "pessoa");
  
  
    exchange.getBody().forEach((pessoa) -> {
      System.out.println("RestTemplate -----------> " + pessoa.toString());
    });
  }
}

class Pessoa {

  private Long id;
  private String nome;

  public Pessoa() {
  }

  public Pessoa(String nome) {
      this.nome = nome;
  }

  public Pessoa(Long id, String nome) {
      this.id = id;
      this.nome = nome;
  }
  
  public Long getId() {
      return id;
  }
  public void setId(Long id) {
      this.id = id;
  }

  public String getNome() {
      return nome;
  }
  
  public void setNome(String nome) {
      this.nome = nome;
  }

  @Override
  public String toString() {
    return "Pessoa {id: " + id + ", nome: " + nome + '}';
  }
  
}